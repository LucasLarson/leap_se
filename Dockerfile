FROM debian:stretch

MAINTAINER LEAP Encryption Access Project <sysdev@leap.se>
LABEL Description="Run amber server behind apache to serve the LEAP website"

ENV DEBIAN_FRONTEND noninteractive

COPY . /var/www/leap-website/

RUN apt-get update \
  && apt-get -y dist-upgrade \
  && apt-get install -y --no-install-recommends \
    apache2 \
    rubygems \ 
    ruby-dev \ 
    zlib1g-dev \
    build-essential \
    git

RUN rm -rf /var/lib/apt/lists/*

# Install amber
RUN gem install amber --no-ri --no-rdoc

# Generate the static html files (into public/)
RUN cd /var/www/leap-website && amber rebuild

#### Setup apache

# Run on a non-priviledge port
RUN sed -i -e 's|Listen 80|Listen 8080|' /etc/apache2/ports.conf
RUN sed -i -e 's|VirtualHost \*:80|VirtualHost \*:8080|' /etc/apache2/sites-enabled/000-default.conf

# Set the document root
RUN sed -i -e 's|DocumentRoot /var/www/html|DocumentRoot /var/www/leap-website/public|' /etc/apache2/sites-enabled/000-default.conf

# Set the amber-specific configuration for the directory so that the indexes and .htaccess will work
RUN sed -i -e 's|</VirtualHost>|     <Directory "/var/www/leap-website/public"> \n        AllowOverride FileInfo Indexes Options=All,MultiViews \n        Require all granted \n     </Directory> \n</VirtualHost>|' /etc/apache2/sites-enabled/000-default.conf

# Enable the necessary modules
RUN /usr/sbin/a2enmod rewrite headers

# Create the directories apache needs, these must be there for unpriviledged apache to run
RUN mkdir /var/run/apache2 /var/lock/apache2 \
    && chown :0 /var/log/apache2 /var/run/apache2 /var/lock/apache2 \
    && chmod 774 /var/log/apache2 /var/run/apache2 /var/lock/apache2

# Make the logs be displayed in the openshift logs tab
RUN ln -sf /dev/stdout /var/log/apache2/access.log \
       && ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log \
       && ln -sf /dev/stderr /var/log/apache2/error.log

# Run apache in the foreground
CMD ["apache2ctl", "-D", "FOREGROUND"]

