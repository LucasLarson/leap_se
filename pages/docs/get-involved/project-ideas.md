@title = "Project Ideas"
@summary = "Ideas for discrete, unclaimed development projects that would greatly benefit the LEAP ecosystem."

Interested in helping with LEAP? Not sure where to dive in? This list of project ideas is here to help.

These are discrete projects that would really be a great benefit to the LEAP development effort, but are separate enough that you can dive right in without stepping on anyone's toes.

If you are interested [contact us on IRC or the mailing list](communication). We will put you in touch with the contact listed under each project.

If you have your own ideas for projects, we would love to hear about it!

Bitmask Client Application
=======================================

Android
----------------------------------------------

### Improve logging in Log view.

Add a search for strings and include color highlighting for different log types.

* Contact: cyberta
* Skills: Android programming

### Review and improve the right-to-left layouts.

Improve User Experience for rigth-to-left languages like Arabic, Farsi and Hebrew.

* Contact: cyberta
* Skills: Android programming

### Improve accessibility.

Review accessibility and fix missing content descriptions

* Contact: cyberta
* Skills: Android programming

### Research on GPS spoofing.

* Research and document mechanisms based on other open source location spoofing apps
* Implement a prototype location provider that is capable to spoof the real location of a user
* Research, document and implement prototypically the communication/interaction between the location provider and the VPN
(Location provider shall be used if VPN is up and the corresponding setting was enenabled in Bitmask) 

* Contact: cyberta
* Skills: Android programming

### Dynamic OpenVPN configuration

Currently the Android app chooses which VPN gateway to connect to based on the nearest timezones by time offset and establishes a configuration for connecting to it by a biased selection of options (ports, protocols, etc) from the set declared by the provider through the API. For cases where a gateway is unavailable or a network is restricting traffic that our configuration matches (e.g. UDP out to port 443), being able to attempt different configurations or gateways would help finding a configuration that worked.

* Contact: cyberta
* Difficulty: Easy
* Skills: Android programming

### Ensure OpenVPN fails closed

For enhanced security, we would like the VPN on Android to have the option of blocking all network traffic if the VPN dies or when it has not yet established a connection. Network traffic would be restored when the user manually turns off the VPN or the VPN connection is restored.

We've implemented the blocking VPN as a solution for non-rooted devices and we support Android's (since A. Oreo / A. 8)  Blocking-VPN System setting. But the blocking VPN option does not play well with the setup of shapeshifter-dispatcher yet: shapeshifter-dispatcher needs to connect before OpenVPN, but Android blocks traffic until OpenVPN is up. Solutions for that problem needs to be investigated. Also, for pre-Andoid 8 *rooted* devices we could implement an iptables-based solution.

* Difficulty: Easy
* Skills: Android programming

## Improved UX

There are several areas where the user experience could be improved, particulary where it comes to our attempts to block unencrypted traffic.

* Priority: Medium
* Difficulty: Depends
* Skills: Android programming

Generic
---------------------------------------

VPN
----------------------------------------

### Gateway selection

Currently, the background bitmask daemon allows for gateway selection, but the current UI doesn't expose this. Consuming the information that the backend exposes, new widgets should be created in the Bitmask UI to allow the user to select the gateways, display different flags depending on the one that's being used, or display the gateways in a map.

* Contact: kali, meskio
* Priority: high
* Difficulty: easy
* Skills: JavaScript.

### Graph traffic statistics

The VPN service provides usage stats in a json document. A new widget is needed that plots both upload and download traffic along time, and also clearly displays when VPN is switched off.

* Contact: kali, meskio
* Priority: high
* Difficulty: easy
* Skills: JavaScript.

### OpenVPN usage improvements

Bitmask VPN works fine, but has many parameters configured statically like tcp and IPv4. It will be nice to acknowledge if the provider supports tcp and/or udp, try first (if supported) with udp and fallback into tcp if it can't connect. The providers can only provide IPv4 addresses to connect to, having support to stablish connections into the providers using IPv6 will be a nice thing to have.

The current firewall blocks all the IPv6 traffic from being routed over the VPN. There is new sources of privacy leaks from using IPv6 that were not existing into IPv4. It will be desirable to have some support for IPv6 in Bitmask VPN, allowing it and investigating what kind of firewall is needed to prevent IPv6 leaks.

Some ideas to improve security of the current VPN settings also involve coordinated changes between the platform and the client, so changes should be tested coordinatedly in both sides. Some topics in this category in the roadmap are: improving the ciphersuit selection, require a minimum tls version, renegotiate tls keys more often, blocking outside dns and implementing obfuscation and other circumvention measures against VPN blocking.

* Contact: meskio, kali, micah
* Priority: medium
* Difficulty: medium
* Skills: python, networking, openvpn, iptables


### iOS port of Bitmask

A basic port of Bitmask to iOS needs to mimick the behavior of the Android app: downloading configuration information from providers, building the OpenVPN command, and setting up the VPN.

* Contact: kali, meskio
* Priority: low
* Difficulty: medium
* Skills: iOS development.

Email
---------------------------------------

### Apple Mail plugin

We have an extension for Thunderbird to autoconfigure for use with Bitmask. It would be great to do the same thing for Apple Mail. [Some tips to get started](http://blog.adamnash.com/2007/09/17/getting-ready-to-write-an-apple-mailapp-plug-in-for-mac-os-x/) and a "links to many existing Mail.app plugins"[http://www.tikouka.net/mailapp/]

* Contact: drebs
* Priority: Low
* Difficulty: Low
* Skills: MacOS programming, Objective-C or Python (maybe other languages too?)

### Microsoft Outlook plugin

We have an extension for Thunderbird to autoconfigure for use with Bitmask. It would be great to do the same thing for Outlook.

* Contact: drebs
* Priority: Low
* Difficulty: Low
* Skills: Windows programming

Soledad Client
---------------------------

### Soledad port

[[Soledad]] is our synchronized, client-encrypted, searchable database. It is written in Python, based on the Python implementation of U1DB (U1DB has similar features to Soledad, but has no encryption). There is also a C version of U1DB called libu1db. This project would be incrementally replace portions of the Python implementation with a version that can be compiled in order to make binding available on Android and iOS.

* Contact: drebs, kali
* Difficulty: Hard
* Priority: High
* Skills: C/C++, using crypto libraries correctly, test driven development.

### Soledad standalone app

Soledad has been coded with the use-case of email as its primary goal. However, it is a generic encrypted and syncable storage, that could be useful to a wider community. In order for soledad to be useful, there are some parts that need to be decoupled from other pieces of the LEAP Platform (including a lightweight implementation of the token-based srp authentication system that can be configured in the absence of couchdb or the LEAP webapp). Once this is done, it would be very easy to code a demo application that showcases how the Soledad library can be used. Interesting project ideas are either a password manager, or an address book (see the section "New Services").

* Contact: kali, drebs
* Difficulty: medium
* Priority: High
* Skills: python, Qt, test driven development.

### JSON1 extension backend for SQLCipher

After U1DB was written, SQLite featured a JSON extension. This would greatly simplify the sqlite backend for Soledad, since we can store json documents directly into sqlite, and create complex indexes without the ned to use intermediate tables. 

* Contact: drebs, kali
* Difficulty: hard
* Priority: High
* Skills: SQL, python or c.

Linux
---------------------------

### Package application for non-Debian linux flavors

The Bitmask client application is entirely ported to Debian, with every dependency library now submitted to unstable. However, many of these packages are not in other flavors of linux, including RedHat/Fedora, SUSE, Arch, Gentoo.

* Contact: kali, micah
* Difficulty: Medium
* Skills: Linux packaging

### Package application for BSD

The Bitmask client application is entirely ported to Debian, with every dependency library now submitted to unstable. However, many of these packages are not in *BSD.

* Contact: kali
* Difficulty: Medium
* Skills: BSD packaging


Windows
-------------------------------

### Code signing on Windows

The bundle needs to be a proper signed application in order to make it safer and more usable when we need administrative privileges to run things like OpenVPN.

* Contact: kali, meskio
* Difficulty: Easy to medium
* Skills: Windows programming

### Proper privileged execution on Windows

Right now we are building OpenVPN with a manifest so that it's run as Administrator. Perhaps it would be better to handle this with User Account Control.

* Contact: kali, meskio
* Difficulty: Medium
* Skills: Windows programming

### Prevent DNS leakage on Windows

Currently, we block DNS leakage on the OpenVPN gateway. This works, but it would be better to do this on the client. The problem is there are a lot of weird edge cases that can lead to DNS leakage. See [dnsleaktest.com](http://www.dnsleaktest.com/) for more information.

* Contact: kali, meskio
* Difficulty: Medium
* Skills: Windows programming

### Add Windows support for Soledad and all the different bundle components

We dropped Windows support because we couldn't keep up with all the platforms, Windows support should be re-added, which means making sure that the gpg modules, Soledad and all the other components are written in a proper multiplatform manner.

* Contact: kali, drebs
* Difficulty: Easy to Medium
* Skills: Windows programming, Python

### Create proper Windows installer for the bundle

We are aiming to distributing bundles with everything needed in them, but an amount of users will want a proper Windows installer and we should provide one. There is some previous work involving building a bitmask installer from within linux, using docker, wine and MinGW.

* Contact: kali
* Difficulty: Medium
* Skills: Windows programming

### Document how to build everything with Visual Studio Express

All the python modules tend to be built with migw32. The current Windows bundle is completely built with migw32 for this reason. Proper Windows support means using Visual Studio (and in our case, the Express edition, unless the proper licenses are bought).

* Contact: kali
* Difficuty: Medium to Hard
* Skills: Windows programming

### Support Windows 64bits

We have support for Windows 32bits, 64bits seems to be able to use that, except for the TAP driver for OpenVPN. So this task is either really easy because it's a matter of calling the installer in a certain way or really hard because it involves low level driver handling or something like that.

* Contact: kali
* Difficulty: Either hard or really easy.
* Skills: Windows programming


Installer and Build Process
----------------------------------------------

### Reproducible builds with Gitian for bundles

We rely on a group of binary components in our bundles, these include libraries like zmq, Qt, PySide, or openssl, among many others. All these should be built in a reproducible way in order to be able to sign the bundles from many points without the need to actually having to send the bundle from the main place it gets built to the rest of the signers. This will also allow a better integration with our automatic updates infrastructure.

* Contact: kali
* Difficulty: Medium to hard


New Services
----------------------------------

### Password keeper

There are multiple password keepers that exist today, but they don't necessarily have a way to sync your passwords from device to device. Building a Soledad backed password keeper would solve all these problems implicitly, it's only a matter of UI and random password generation.

* Contact: drebs, elijah
* Priority: Low
* Difficulty: Easy to medium
* Skills: Python

### Notepad app

This idea is basically a simple note pad application that saves all its notes as Soledad documents and syncs them securely against a Soledad server.

* Contact: kali, drebs
* Priority: Low
* Difficulty: Easy to medium
* Skills: Python

Miscellaneous
-------------------------------

### Token-based user registration

The idea is to allow or require tokens in the new user signup process. These tokens might allow to claim a particular username, give you a credit when you sign up, allow you to sign up, etc.

* Dependency: token-based signup in webapp API.
* Contact: elijah
* Difficulty: Easy
* Skills: Python

### General QA

One thing that we really need is a team of people that is constantly updating their versions of the code and testing the new additions. Basic knowledge of Git would be needed, and some really basic Python.

* Contact: mcnair, elijah
* Difficulty: Easy to medium, depending on the QA team that is managed.

### Translations

Do you speak a language that's not English? Great! We can use your help! We are always looking for translators for every language possible.

* Contact: kali, meskio
* Difficulty: Easy

### Support for OpenPGP smart cards

A really nice piece of hardware is OpenPGP smart cards. What would be needed is a way to save the generated key in the smart card instead of in Soledad (or both, should be configurable enough) and then migrate the regular OpenPGP workflow to support these change.

* Contact: drebs
* Difficulty: Medium

### Device blessing

Add the option to require a one-time code in order to allow an additional device to be synchronized with your account.

* Contact: elijah
* Difficulty: Hard
* Skills: Python

### Push notifications from the server

There are situations where the service provider you are using through the bitmask client might want to notify some event to all its users. May be some downtime, or any other problems or situations. There should be an easy way to push such notifications to the client.

* Contact: elijah
* Difficulty: Easy
* Skills: Python

### Quick wipe of all data

Some users might be in situations where being caught with software like OpenVPN is illegal or basically just problematic. There should be a quick way to wipe the existence of the whole bundle and your identity from provider.

* Contact: kali, elijah
* Difficulty: Easy
* Skills: Python


LEAP Platform
===========================

Soledad Server
---------------------------

### Add support for quota

Soledad server only handles authentication and basic interaction for sync. The recent blobs implementation has a very basic implementation of user quotas, but it needs to be made more efficient and cover some corner cases.

* Contact: drebs, kali.
* Priority: Medium
* Difficulty: easy
* Skills: Python

### Add support for easier soledad server deployment

Currently Soledad relies on a fairly complex CouchDB setup. It can be deployed with just one CouchDB instance, but may be if you are just using one instance you might be good enough with SQLite or other easy to setup storage methods. The same applies to authentication, may be you want a handful of users to be able to use your Soledad sever, in which case something like certificate client authentication might be enough. So it would be good to support these non-scalable options for deploying a Soledad server.

* Contact: drebs
* Priority: Low
* Difficulty: Medium
* Skills: Python

### A soledad management tool

Bootstrapping Soledad and being able to sync with it is not a necessarily easy task, you need to take care of auth and other values like server, port, user id. Having an easy to use command line interface application that can interact with Soledad would ease testing both on the client as on the server.

* Contact: drebs
* Priority: Low
* Difficulty: Easy
* SKills: Python

### Pluggable authentication system

Currently, soledad depends on couchdb and the LEAP platform to be deployed. For testing purposes, and for lightweight deployments, a simpler token-based authentication system would be very useful.

* Contact: drebs, kali
* Priority: medium
* Difficulty: easy
* Skills: python, twisted.

### Federated Soledad

Currently, each user's Soledad database is their own and no one else ever has access. It would be mighty useful to allow two or more users to share a Solidad database. This would allow us to use Soledad for a shared calendar, for example.

* Contact: drebs, elijah
* Difficult: Hard
* Skills: Python

DNS
--------------------------------

### Add DNSSEC entries to DNS zone file

We should add commands to the leap command line tool to make it easy to generate KSK and ZSK, and sign DNS entries.

* Contact: elijah, micah, varac
* Difficulty: Easy
* Skills: Ruby

### Add DANE entries to DNS zone file

Every node one or more server certificates. We should publish these using DANE.

* Contact: elijah, micah, varac
* Difficulty: Easy

### Add DKIM entries to DNS zone file

We need to generate and publish [DKIM](https://en.wikipedia.org/wiki/DKIM) keys.

* Contact: elijah, micah, varac
* Difficulty: Easy

OpenVPN
-----------------------------------

### OpenVPN with ECC PFS support

Currently, OpenVPN gets configured to use a non-ECC DH cipher with perfect forward secrecy, but it would be nice to get it working with an Elliptical Curve Cipher. This greatly reduces the CPU load of the OpenVPN gateway.

* Contact: elijah, varac
* Priority: Low
* Difficulty: Low
* Skills: OpenVPN, X.509

Email
--------------------------

### Mailing list support

Managing an encrypted mailing list is too difficult for most users. Even worse, existing solutions store a private key for the list on the server. It would be very useful to have a simple, end-to-end encrypted mailing list system that anyone could use.

Our idea is a simple API that allows the client to query the subscriber list and set a new subscriber list. This command would include the addresses that should be subscribed to the list, and the full fingerprint of the key to use for each address. So long as this command was signed by a subscriber's private key, the list server accepts the new subscriber list. As an next iteration, the software could support the ability to designate that only "admins" have this ability. The client application would then include a simple form for modifying the list of subscribers. There would be no archives and no web interface.

Another possibility is something like, [PSELS](http://www.ncsa.illinois.edu/people/hkhurana/ICICS.pdf). PSELS uses OpenPGP in a novel way to achieve proxy re-encryption, allowing for a mailing list in which the server does not ever have access to messages in cleartext, but subscribers don't need to encrypt each message to the public key of all subscribers. However, the setup is incredibly complex, and requires special keys.

* Contact: elijah
* Priority: Medium
* Difficulty: Hard
* Skills: Cryptography, Python


LEAP Webapp
============================

### Add support for newsletter

Sometimes simple push notifications aren't enough, you may want to mail a newsletter to your users or more descriptive notifications, it should be possible for an administrator of a provider to use the webapp to quickly send mail to all its users.

* Contact: jessi, azul, elijah
* Difficulty: Easy

### Add support for quota

Description: Once the Soledad server quota enforcement code is in place (there is some preliminar implementation in soledad using blobs), it would be good to have the ability to configure the quota for a user and check the user's quota via the webapp.

* Dependency: Soledad server quota enforcement.
* Contact: azul, elijah
* Difficulty: Easy
* Skills: Ruby

### Add a status system

Having a simple way for users to be able to visit the website to see if there are any system problems they should be aware of would be very useful. Exposing to the administrators a way to add a system status notification, and set a critical priority level and then the newest message would be displayed on the top of a status page, using different colors for the severity levels. 

* Contact: guido, azul, elijah, micah
* Difficulty: Easy
* Skills: Rails

### Internal knowledge base

When a user reports a problem, suggest related help pages that could solve their problem. Many of the more useful help ticket systems out there will do this, its a great way to cut down on requests for help that are handled by the documentation. If the suggested help pages could be additionally triggered by some tags, enabling the provider to hint the suggestions based on their experiences would be very helpful.

* Contact: elijah, azul
* Difficulty: Easy to medium
* Skills: Ruby and Javascript

### Dynamic form validation

Any form element should have dynamic feedback for the user. For example, if you tried to add the username "hello im an username with spaces", a message should come up on the page that says that you cannot have a username with spaces. 

* Contact: elijah, azul
* Difficulty: Easy
* Skills: Ruby and Javascript

### Help ticket routing

If there is a problem with the bitmask application itself, it doesn't make sense to send that to the provider, it should be sent to the bitmask development team. This this could be something that could be changed for different classes of help tickets, and  something that links to other ticketing systems.

* Contact: elijah, azul
* Difficulty: Medium
* Skills: Ruby
