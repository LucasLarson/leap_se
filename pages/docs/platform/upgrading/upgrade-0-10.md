@title = 'Upgrade to 0.10'
@toc = false

Upgrading to Platform 0.10
---------------------------------------------

If you have a node with the service 'tor' defined, you will need to change it to
be either 'tor-relay', or 'tor-exit'. Look in your provider directory under the
nodes directory for any .json file that has a 'services' section with 'tor'
defined, change that to the correct tor service you are wanting to deploy.

Make sure you have the correct version of leap_cli

    workstation$ sudo gem install leap_cli --version=1.9

If you are upgrading from a version previous to 0.9, please follow those upgrade
instructions before upgrading to 0.10.

Prepare your platform source by checking out the 0.10.x branch:

    workstation$ cd leap_platform
    workstation$ git fetch
    workstation$ git checkout 0.10.x

Then, deploy:

    workstation$ cd $PROVIDER_DIR
    workstation$ leap deploy
    workstation$ leap test
After deployment, if the leap test does not succeed, you should
investigate. Please see below for some post-deployment upgrade steps that you
may need to perform.

Starting with Soledad Server 0.9.0, the CouchDB database schema was changed to
improve speed of the server side storage backend. If you provided email, you
will need to run the migration script, otherwise it is unnecessary. Until you
migrate, soledad will refuse to start.

To run the migration script, do the following (replacing $PROVIDER_DIR,
$COUCHDB_NODE, $MX_NODE, and $SOLEDAD_NODE with your values):

First backup your couchdb databases, just to be safe. NOTE: This can take some
time and will place several hundred megabytes of data into
/var/backups/couchdb. The size and time depends on how many users there are on
your system. For example, 15k users took approximately 25 minutes and 308M of
space:
    workstation$ leap ssh $COUCHDB_NODE
    server# cd /srv/leap/couchdb/scripts
    server# ./cleanup-user-dbs
    server# time ./couchdb_dumpall.sh

 Once that has finished, then its time to run the migration:

    workstation$ cd $PROVIDER_DIR
    workstation$ leap run 'systemctl leap_mx stop' $MX_NODE
    workstation$ leap run --stream '/usr/share/soledad-server/migration/0.9/migrate.py --log-file /var/log/leap/soledad_migration --verbose --do-migrate' $SOLEDAD_NODE
    wait for it to finish (will print DONE)
    rerun if interrupted
    workstation$ leap deploy
    workstation$ leap test

Known Issues:

If you have been deploying from our master branch (ie: unstable code), you might
end up with a broken sources line for apt. If you get the following:
    WARNING: The following packages cannot be authenticated!
Then you should remove the files on your nodes inside
/var/lib/puppet/modules/apt/keys and deploy again. (#8862, #8876)

* When upgrading, sometimes systemd does not report the correct state of a
  daemon. The daemon will be not running, but systemd thinks it is. The symptom
  of this is that a deploy will succeed but `leap test` will fail. To fix, you
  can run `systemctl stop DAEMON` and then `systemctl start DAEMON` on the
  affected host (systemctl restart seems to work less reliably).

Includes:

* leap_web: 0.9.2
* nickserver: 0.10.0
* leap-mx: 0.10.1
* soledad-server: 0.10.5

Commits: https://0xacab.org/groups/leap/milestones/platform-010?title=Platform+0.10

For details on about all the changes included in this release please consult the
[LEAP platform 0.10 milestone](https://0xacab.org/leap/platform/milestones/7 ).



Always a good idea to check if you have the latest version of leap_cli:

    workstation$ sudo gem install leap_cli --version=1.9

If you don't want to install using 'sudo':

    workstation$ gem install --user-install leap_cli --version=1.9
    workstation$ PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"

