@title = 'So long Google Summer of Code!'
@author = 'kwadronaut'
@posted_at = '2019-02-03'
@more = true
@preview_image = '/img/pages/GSoC-icon-192.png'

We've enjoyed participating in the Google Summer of Code in the past and will probably do it again in the future. Alas, this year we will not participate in GSoC. Prioritizing ideas, mentoring students and being on top of the organizational aspects all requires time and energy that the current team would like to devote to our core pieces. Even with the talent that wants to dive into this project during their school breaks, time remains limited.

However, this shouldn't hold anyone back to contribute in their own time! Our Windows firewall is advancing, snap packages get pushed almost monthly (tailored for Riseup at [snapcraft](https://snapcraft.io/riseup-vpn)), we're experimenting with [pluggable transports](https://www.pluggabletransports.info/)… input is welcome in many areas: from translations to code over documentation to general praise and feedback.

* Code & Bugtracker: https://0xacab.org/leap
* Android translations: https://www.transifex.com/otf/bitmask-android/dashboard/
* Desktop translations: https://www.transifex.com/otf/bitmask/RiseupVPN/
* Mailinglist: leap-discuss@lists.riseup.net
* Chat: ircs://irc.freenode.org/#leap if you don't have an irc client, you can use some gateway like [Matrix](https://about.riot.im/)
* ♥ donation: <https://leap.se/en/about-us/donate>
